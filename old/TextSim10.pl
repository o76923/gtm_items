use warnings;
use strict;
use open ':encoding(utf8)';
use IO::Handle;

## Run this file as follows:
##  perl TextSim.pl input.txt out.txt
## This version uses the word similarity >= average+stdDev per ROW

## there are 2 ARGV
$inf= $ARGV[0];
    open(TYPE_FREQ_INPUT,"<$inf") ||     ##  input file
        die "Can't input $inf $!";
 

$outf=$ARGV[1]; # result"o.txt";
    open(OUTPUT,">$outf") ||
        die "Can't output $outf $!";
$errf = $ARGV[2];
	open(ERRORFILE,">$errf") ||
		die "Can't make error file $errf $!";
ERRORFILE->autoflush;
my $tot_count=0;
$pair_no=0;
# load vocab file
my( $file, $handle, @token, %one, %three );
$file = 'vocab' or die $!;
open( $handle, '<', $file ) or die "\'$file\': ", $!;
while( <$handle> )
{	@token = split /\s/;
	unless( defined $one{ $token[ 0 ] } )
	{	$one{ $token[ 0 ] } = $token[ 1 ];
	}
	undef @token;
}
close $handle;
print "\'$file\': loaded\n";
################################


while ($string=<TYPE_FREQ_INPUT>)    
  {	my( $string1, $string2 ) = split( /[\t\v]/, $string );
  	$pair_no++;
	print "Pair No. $pair_no\n";
	$greater=0;
	$oner=0;

###########################
my @individual_score;
my @arr1=FindToken($string1);
my @arr2=FindToken($string2);

###########################################
### THIS MODULE MAKES SURE SO THAT @arr1 always contains the smaller string
if ($#arr1>$#arr2)
{
	my @temp_arr=@arr1;
	@arr1=@arr2;
	@arr2=@temp_arr;
} 

my $shorter_array_len=$#arr1;
my $longer_array_len=$#arr2;

##################################################
### STEP 1
### This part is for determining the exact matching




if ($#arr1<=$#arr2) 
{
	$i=0;
	
	while ($i<=$#arr1)
	{	$tok1=$arr1[$i];
		$j=0;
		while ($j<=$#arr2)
		{
			$tok2=$arr2[$j];
			if ($tok1 eq $tok2)
			{
				@individual_score[$#individual_score+1]=1;
				@arr1=RemoveAnArrayElem(\@arr1,$tok1);
				@arr2=RemoveAnArrayElem(\@arr2,$tok2);
				$i--;$j--;last;  ##### this "last" is a key word 
			}
			$j++;
		}
		$i++;
	}

}



##################################################
#### STEP 3(0)
#### FIND PAIR WORDS THAT WILL BE WRITTEN IN A FILE LIKE mc.txt THAT WILL BE THE INPUT FOR ALISTAIR'S PROGRAM
my @array2Ds;
my $array2D_row=$#arr1;
my $array2D_col=$#arr2;
my @temp_arr1=@arr1;
my @temp_arr2=@arr2;

my $fn=1;

my  %mHash = ();

#$outf="mc.txt"; #"o.txt";
#	   $mode=">";
#  	    open(MCTXT,'>', 'mc.txt') ||
#       	    die "Can't output $outf $!";

my@mctxt;
 
if ($#arr1!=-1 && $#arr2!=-1)
{
	$i=0;
	###### FIND ALL MATCHING MATRIX	
	while ($i<=$#arr1)
	{	$tok1=$arr1[$i];
		$j=0;
		
		while ($j<=$#arr2)
		{
			$tok2=$arr2[$j];  
			
			if ($mHash{$tok1}{$tok2}==0 && $mHash{$tok2}{$tok1}==0)
			{ $mHash{$tok1}{$tok2}=1;
				#print MCTXT "$tok1 $tok2\n";
				push @mctxt, "$tok1 $tok2\n"
#print "$tok1 $tok2\n";
			}  ###  RetSemSimilarity($tok1,$tok2); recently changed
				
					
			$j++;
		}
		$i++;
	}

		
}
######################################################
#close MCTXT;
########################################################

# -------------------------------------------------------------------------------------------
# This program takes "mc.txt" file as input and the file contains pair of words in a single line separated by space
# and the output of the program is a file named "pairSim.txt" that contains pair of words and their similarity scores in a single line
#############################################################################


#my $inputf="mc.txt";
#	open(INPUT,"<$inputf") ||
#        die "Can't input $inputf $!";

#$outf="pairSim.txt"; 
#
#	    $mode=">";
#   		open(PAIRSIM,"$mode$outf") ||
#        	die "Can't output $outf $!";

my( $main_string, @pairSim );

#while ($main_string=<INPUT>)    
foreach $main_string( @mctxt )
{
	 $min_length=5000000000;

	my @str=FindToken($main_string);
	my $str1=@str[0];		## "car ";
	my $str2=@str[1];		##"vehicle ";
	my $car_frequency = $one{ $str1 };
	my $automobile_frequency = $one{ $str2 };
###################################################################################
######################			#########################
######################  For Tri-gram 	#########################
######################			#########################

#my $str1="food ";
#my $str2="fruit ";


my $hash_ref1={};
my $hash_ref2={};
my $hash_ref_common1={};
my $hash_ref_common2={};


my $fname1=Find_3gmFile($str1);
my $fname2=Find_3gmFile($str2);

my$car_automobile_Tri_frequency = 0;
if( exists $three{ $fname1 } )
{	if( defined $three{ $fname1 }{ $str1 }{ $str2 } )
	{	$car_automobile_Tri_frequency = $three{ $fname1 }{ $str1 }{ $str2 };
	}
}else
{	if( open( $handle, '<', "$fname1" ) )
	{	while( <$handle> )
		{	@token = /^([^ ]*) ([^ ]*) ([^\t]*)\t(\d*)/;
			if( $token[ 1 ] =~ /^\p{XPosixAlnum}*$/ )
			{	if( defined $three{ $fname1 }{ $token[ 0 ] }{ $token[ 2 ] } )
				{	$three{ $fname1 }{ $token[ 0 ] }{ $token[ 2 ] } = $three{ $fname1 }{ $token[ 0 ] }{ $token[ 2 ] } + $token[ 3 ];
				}else
				{	$three{ $fname1 }{ $token[ 0 ] }{ $token[ 2 ] } = $token[ 3 ];
				}
			}
		}
		close $handle;
		print "\'$fname1\': loaded\n";
		if( defined $three{ $fname1 }{ $str1 }{ $str2 } )
		{	$car_automobile_Tri_frequency = $three{ $fname1 }{ $str1 }{ $str2 };
		}
	}else
	{	#print "\'$fname1\': ", $!, "\n";
	}
}
my$automobile_car_Tri_frequency = 0;
if( exists $three{ $fname2 } )
{	if( defined $three{ $fname2 }{ $str2 }{ $str1 } )
	{	$automobile_car_Tri_frequency = $three{ $fname2 }{ $str2 }{ $str1 };
	}
}else
{	if( open( $handle, '<', "$fname2" ) )
	{	while( <$handle> )
		{	@token = /^([^ ]*) ([^ ]*) ([^\t]*)\t(\d*)/;
			if( $token[ 1 ] =~ /^\p{XPosixAlnum}*$/ )
			{	if( defined $three{ $fname2 }{ $token[ 0 ] }{ $token[ 2 ] } )
				{	$three{ $fname2 }{ $token[ 0 ] }{ $token[ 2 ] } = $three{ $fname2 }{ $token[ 0 ] }{ $token[ 2 ] } + $token[ 3 ];
				}else
				{	$three{ $fname2 }{ $token[ 0 ] }{ $token[ 2 ] } = $token[ 3 ];
				}
			}
		}
		close $handle;
		print "\'$fname2\': loaded\n";
		if( defined $three{ $fname2 }{ $str2 }{ $str1 } )
		{	$automobile_car_Tri_frequency = $three{ $fname2 }{ $str2 }{ $str1 };
		}
	}else
	{	#print "\'$fname2\': ", $!, "\n";
	}
}

#print "CAR AUTOMOBILE TRIGRAM FREQ $car_automobile_Tri_frequency \n AUTOMOBILE CAR TRIGRAM FREQ $automobile_car_Tri_frequency\n";



 
my $sim_Trigram=0;

if (($car_frequency>0) && ($automobile_frequency>0))
{
	my $min_car_auto;
	my $max_car_auto; 
	if ($car_frequency>=$automobile_frequency)
	{
		$min_car_auto=$automobile_frequency;
		$max_car_auto=$car_frequency;
	}
	else
	{
		$max_car_auto=$automobile_frequency;
		$min_car_auto=$car_frequency;
	}
	my $avg= ($car_automobile_Tri_frequency + $automobile_car_Tri_frequency)/2;
	

	my $the_freq= 19401194714;	### `the' appears  19401194714 times

	my $numerator=($avg*$the_freq*$the_freq)/($car_frequency*$automobile_frequency*$min_car_auto);
	

	if ($numerator<=1)
	{
		$numerator=1.01;

	}
	if ($avg==0)
	{
		$numerator=1;
	}

	

       eval {$sim_Trigram=log($numerator)/(-2*log($min_car_auto/$the_freq))};
	   if($@){
		   print ERRORFILE "$pair_no\n";
		   $sim_Trigram="NULL";
		}else{
			$sim_Trigram=log($numerator)/(-2*log($min_car_auto/$the_freq));
		}
    ##    $sim=log(($avg*$the_freq*$the_freq)/($car_frequency*$automobile_frequency*$max_car_auto*$min_car_auto))/-log($max_car_auto/$the_freq);

}

###################################################################################
##################################################################


push @pairSim, sprintf( "$str1 $str2 %.2f\n", $sim_Trigram );

 my $hash_ref1 = {}; ### clear hasr_ref1  
 my $hash_ref2 = {}; ### clear hasr_ref2
 my $hash_ref_common1 = {};
 my $hash_ref_common2 = {};

} ## end while
#close INPUT;
#close PAIRSIM;

############################################################################################
###  LOCATE THE FILE NAME IN GOOGLE Web 1T 3gm FILES based on the string
#############################################################################################
sub Find_3gmFile 
{	my($str)=@_;
	my($fName);
	

	if ($str ge "& UnifyPow \"	" && $str le "( Men were	") {$fName="3gm-0001";}
	elsif ($str ge "( Men who	" && $str le "+ * config.sub	") {$fName="3gm-0002";}
	elsif ($str ge "+ * config.table	" && $str le ", Mary Alvarez	") {$fName="3gm-0003";}
	elsif ($str ge ", Mary Alvina	" && $str le ", nude hendrix	") {$fName="3gm-0004";}
	elsif ($str ge ", nude hentai	" && $str le "- Farr 's	") {$fName="3gm-0005";}
	elsif ($str ge "- Farr (	" && $str le "- relational engine	") {$fName="3gm-0006";}
	elsif ($str ge "- relational environment	" && $str le "/ songs )	") {$fName="3gm-0007";}
	elsif ($str ge "/ songs *	" && $str le "10 January --	") {$fName="3gm-0008";}
	elsif ($str ge "10 January .	" && $str le "180 4128 </S>	") {$fName="3gm-0009";}
	elsif ($str ge "180 4128 |	" && $str le "2006 Chocolate -	") {$fName="3gm-0010";}
	elsif ($str ge "2006 Chocolate /	" && $str le "397KB PDF )	") {$fName="3gm-0011";}
	elsif ($str ge "397KB ] </S>	" && $str le "7 Ãº </S>	") {$fName="3gm-0012";}
	elsif ($str ge "7 Ã» ;	" && $str le ": DarkCompass </S>	") {$fName="3gm-0013";}
	elsif ($str ge ": DarkCounter </S>	" && $str le "<S> 1006 Hwy	") {$fName="3gm-0014";}
	elsif ($str ge "<S> 1006 KB	" && $str le "<S> Bell officials	") {$fName="3gm-0015";}
	elsif ($str ge "<S> Bell often	" && $str le "<S> Gloss measurements	") {$fName="3gm-0016";}
	elsif ($str ge "<S> Gloss medium	" && $str le "<S> OUTLET <UNK>	") {$fName="3gm-0017";}
	elsif ($str ge "<S> OUTLET ADS	" && $str le "<S> Thinker <UNK>	") {$fName="3gm-0018";}
	elsif ($str ge "<S> Thinker >	" && $str le "<S> nent military	") {$fName="3gm-0019";}
	elsif ($str ge "<S> nent model	" && $str le "<UNK> Coastpath :	") {$fName="3gm-0020";}
	elsif ($str ge "<UNK> Coastpath Nearby	" && $str le "A MEMBER and	") {$fName="3gm-0021";}
	elsif ($str ge "A MEMBER box	" && $str le "Aitchison ( Oxford	") {$fName="3gm-0022";}
	elsif ($str ge "Aitchison ( Sat	" && $str le "Auckland Festival 05	") {$fName="3gm-0023";}
	elsif ($str ge "Auckland Festival </S>	" && $str le "Bingo - Help	") {$fName="3gm-0024";}
	elsif ($str ge "Bingo - Here	" && $str le "CEOS Events FAQs	") {$fName="3gm-0025";}
	elsif ($str ge "CEOS FORM COALITION	" && $str le "Chicago Membership News	") {$fName="3gm-0026";}
	elsif ($str ge "Chicago Membership Posted	" && $str le "CopÃ¡n Mosaics Project	") {$fName="3gm-0027";}
	elsif ($str ge "CopÃ¡n Ruinas (	" && $str le "Deep & Simple	") {$fName="3gm-0028";}
	elsif ($str ge "Deep & Skin	" && $str le "ESSENTIALS II </S>	") {$fName="3gm-0029";}
	elsif ($str ge "ESSENTIALS IN ONE	" && $str le "FOAM - 888	") {$fName="3gm-0030";}
	elsif ($str ge "FOAM - <UNK>	" && $str le "From southeast corner	") {$fName="3gm-0031";}
	elsif ($str ge "From southeast star	" && $str le "GualeguaychÃº ( Argentina	") {$fName="3gm-0032";}
	elsif ($str ge "GualeguaychÃº ( ar	" && $str le "Hotel In Kurdistan	") {$fName="3gm-0033";}
	elsif ($str ge "Hotel In Kusadasi	" && $str le "Installing xerces -	") {$fName="3gm-0034";}
	elsif ($str ge "Installing xforms .	" && $str le "KIDS DESIGN Current	") {$fName="3gm-0035";}
	elsif ($str ge "KIDS DESIGNER CLOTHES	" && $str le "Lexapro 10/31/05 (	") {$fName="3gm-0036";}
	elsif ($str ge "Lexapro 10mg (	" && $str le "Making Noise '	") {$fName="3gm-0037";}
	elsif ($str ge "Making Noise (	" && $str le "Moment With Gael	") {$fName="3gm-0038";}
	elsif ($str ge "Moment With God	" && $str le "Nice ( about	") {$fName="3gm-0039";}
	elsif ($str ge "Nice ( adamr.	" && $str le "Our Honesty Policy	") {$fName="3gm-0040";}
	elsif ($str ge "Our Honey &	" && $str le "Phaser 350 Phaser	") {$fName="3gm-0041";}
	elsif ($str ge "Phaser 350 Tektronix	" && $str le "Pullets : Number	") {$fName="3gm-0042";}
	elsif ($str ge "Pullets ; </S>	" && $str le "Residency program are	") {$fName="3gm-0043";}
	elsif ($str ge "Residency program at	" && $str le "Sabatini , D.	") {$fName="3gm-0044";}
	elsif ($str ge "Sabatini , DA	" && $str le "Simple Guestbook 2.0	") {$fName="3gm-0045";}
	elsif ($str ge "Simple Guestbook </S>	" && $str le "Students realize they	") {$fName="3gm-0046";}
	elsif ($str ge "Students realized that	" && $str le "Tests mit dem	") {$fName="3gm-0047";}
	elsif ($str ge "Tests module for	" && $str le "Tool Dallas Free	") {$fName="3gm-0048";}
	elsif ($str ge "Tool Dallas Garden	" && $str le "VPD , SQL	") {$fName="3gm-0049";}
	elsif ($str ge "VPD , and	" && $str le "White Leather G	") {$fName="3gm-0050";}
	elsif ($str ge "White Leather Gift	" && $str le "] Logging questions	") {$fName="3gm-0051";}
	elsif ($str ge "] Logging remote	" && $str le "academic and test	") {$fName="3gm-0052";}
	elsif ($str ge "academic and testing	" && $str le "also need compliance	") {$fName="3gm-0053";}
	elsif ($str ge "also need comprehensive	" && $str le "and democratizing ,	") {$fName="3gm-0054";}
	elsif ($str ge "and democratizing .	" && $str le "approached Lena ,	") {$fName="3gm-0055";}
	elsif ($str ge "approached Lenin after	" && $str le "auctions are insured	") {$fName="3gm-0056";}
	elsif ($str ge "auctions are intended	" && $str le "between , public	") {$fName="3gm-0057";}
	elsif ($str ge "between , pure	" && $str le "by : patrikbeno	") {$fName="3gm-0058";}
	elsif ($str ge "by : patriot	" && $str le "ceramics chess sets	") {$fName="3gm-0059";}
	elsif ($str ge "ceramics chlorine free	" && $str le "companion parrots possible	") {$fName="3gm-0060";}
	elsif ($str ge "companion part ,	" && $str le "cracking programs (	") {$fName="3gm-0061";}
	elsif ($str ge "cracking programs )	" && $str le "deserved praise and	") {$fName="3gm-0062";}
	elsif ($str ge "deserved praise as	" && $str le "driveway is permitted	") {$fName="3gm-0063";}
	elsif ($str ge "driveway is plowed	" && $str le "error executing this	") {$fName="3gm-0064";}
	elsif ($str ge "error executing wwmaster.initiate	" && $str le "feet secretary sex	") {$fName="3gm-0065";}
	elsif ($str ge "feet secretary stockings	" && $str le "for nude lesbianas	") {$fName="3gm-0066";}
	elsif ($str ge "for nude lesbians	" && $str le "gas card ...	") {$fName="3gm-0067";}
	elsif ($str ge "gas card 0	" && $str le "hardcore XXX time	") {$fName="3gm-0068";}
	elsif ($str ge "hardcore XXX video	" && $str le "hotels , ideally	") {$fName="3gm-0069";}
	elsif ($str ge "hotels , ideas	" && $str le "in states directly	") {$fName="3gm-0070";}
	elsif ($str ge "in states do	" && $str le "iorque + saudades	") {$fName="3gm-0071";}
	elsif ($str ge "iorr.org , yesterday	" && $str le "la presiÃ³n <UNK>	") {$fName="3gm-0072";}
	elsif ($str ge "la presiÃ³n a	" && $str le "local buildings or	") {$fName="3gm-0073";}
	elsif ($str ge "local buildings such	" && $str le "means officers are	") {$fName="3gm-0074";}
	elsif ($str ge "means officers can	" && $str le "moxie \" .	") {$fName="3gm-0075";}
	elsif ($str ge "moxie \" and	" && $str le "not have NBC	") {$fName="3gm-0076";}
	elsif ($str ge "not have NC	" && $str le "of emotionally laden	") {$fName="3gm-0077";}
	elsif ($str ge "of emotionally loaded	" && $str le "one cause as	") {$fName="3gm-0078";}
	elsif ($str ge "one cause at	" && $str le "output is added	") {$fName="3gm-0079";}
	elsif ($str ge "output is additionally	" && $str le "photos acteress ,	") {$fName="3gm-0080";}
	elsif ($str ge "photos acteress indian	" && $str le "priced proposal </S>	") {$fName="3gm-0081";}
	elsif ($str ge "priced proposal and	" && $str le "raised during 1999	") {$fName="3gm-0082";}
	elsif ($str ge "raised during 2001	" && $str le "research materials )	") {$fName="3gm-0083";}
	elsif ($str ge "research materials *	" && $str le "school right as	") {$fName="3gm-0084";}
	elsif ($str ge "school right at	" && $str le "shows old power	") {$fName="3gm-0085";}
	elsif ($str ge "shows old school	" && $str le "spermine , a	") {$fName="3gm-0086";}
	elsif ($str ge "spermine , and	" && $str le "supplement and advance	") {$fName="3gm-0087";}
	elsif ($str ge "supplement and after	" && $str le "that includes accomplishments	") {$fName="3gm-0088";}
	elsif ($str ge "that includes account	" && $str le "the documents transcribed	") {$fName="3gm-0089";}
	elsif ($str ge "the documents transferred	" && $str le "then will reduce	") {$fName="3gm-0090";}
	elsif ($str ge "then will refer	" && $str le "to access Payroll	") {$fName="3gm-0091";}
	elsif ($str ge "to access Penn	" && $str le "trials will look	") {$fName="3gm-0092";}
	elsif ($str ge "trials will make	" && $str le "valid debts may	") {$fName="3gm-0093";}
	elsif ($str ge "valid debts of	" && $str le "well as brushless	") {$fName="3gm-0094";}
	elsif ($str ge "well as brutal	" && $str le "with two mechanisms	") {$fName="3gm-0095";}
	elsif ($str ge "with two mechanized	" && $str le "| Finite Element	") {$fName="3gm-0096";}
	elsif ($str ge "| Finite IT	" && $str le "É É É	") {$fName="3gm-0097";}
	elsif ($str ge "! \" ''	" && $str le "& Unify Our	") {$fName="3gm-0000";}
	else {$fName="No file matched";}
	
 	return $fName;
}
##########################################################################################################
####################################################################################################################################

#############################

### this module assign all simPair from pairSim.txt file in 2 D hash
#$inf1= "pairSim.txt";  
#                           ## PairSim file
#    open(SIMI,"<$inf1") ||
#       die "Can't input $inf1 $!";

my  %mdimHash = ();
 $x=0;
 my $xxx=1;
foreach $string( @pairSim )    
  {	
	@token=FindToken($string);
	$mdimHash{@token[0]}{@token[1]} = @token[2];
	$x++;
	if ($x == $xxx*500000)
	{print "BiG $x\n"; $xxx++;}
   }

##################################

###################################
#### STEP 3
#### SIMILARITY MATCHING
my @array2Ds;
my $array2D_row=$#arr1;
my $array2D_col=$#arr2;
my @temp_arr1=@arr1;
my @temp_arr2=@arr2;


if ($#arr1!=-1 && $#arr2!=-1)
{
	$i=0;
	###### FIND ALL MATCHING MATRIX	
	while ($i<=$#arr1)
	{	$tok1=$arr1[$i];
		$j=0;
		
		while ($j<=$#arr2)
		{
			$tok2=$arr2[$j];  
			
			if ($mdimHash{$tok1}{$tok2})
			{$ret_value= $mdimHash{$tok1}{$tok2};}  ###  RetSemSimilarity($tok1,$tok2); recently changed
			else
			{$ret_value= $mdimHash{$tok2}{$tok1};}		  
						
			if ($ret_value<0)
				 {$ret_value = 0;}	
			
			 

			if ($ret_value>=0)
			{
				$array2Ds[$i][$j]=$ret_value;
			}
			$j++;
		}
		$i++;
	}

		

}

###############################################################
#################################################################
##### DO THE MAIN CALCULATION
my $array2D_row=$#arr1;
my $array2D_col=$#arr2;


	my $max=1;	
	$done=0;
	while ($done==0 && $max!=0)
	{
		$i=0;
		$max_row=-1;
		$max_col=-1;
		

		while ($i<=$array2D_row)
		{	
			$max=0;
			$j=0;
			while ($j<=$array2D_col)
			{
				
					$max=$max+$array2Ds[$i][$j];
					$max_row=$i;
					$max_col=$j;
				
				$j++;
			}
			my $avg=$max/($max_col+1);

			$max=0;
			$j=0;
			my $square_sum=0;
			while ($j<=$array2D_col)
			{
				
				$square_sum=$square_sum+($array2Ds[$i][$j]-$avg)*($array2Ds[$i][$j]-$avg);
				$j++;
			}

			my $std_dev=sqrt($square_sum/($max_col+1));

			$max=0;
			$j=0;
			my $count=0;
			while ($j<=$array2D_col)
			{
				if ($array2Ds[$i][$j]>=($avg+$std_dev))
				{
					$max=$max+$array2Ds[$i][$j];
					$count++;

				}
				
					
					
				
				$j++;
			}
			if ($count==0) {$count=1;}

#print "Count= $count $max_col\n";
			$max=$max/($count);
			@individual_score[$#individual_score+1]=$max;
			$i++;
		}
		
			
					
		
			$done=1;
		
	}




##################################################
### CALCUALTE FINAL SIMILARITY SCORE
my $sum=0;

foreach my $ele (@individual_score)
{
	$sum=$sum+$ele;
#print "ele = $ele\n";
}


eval {$final_sim_score=  ($sum*($shorter_array_len+1)+$sum*($longer_array_len+1))/(2*($shorter_array_len+1)*($longer_array_len+1))};
	   if($@){
		   print ERRORFILE "$pair_no\n";
		   $final_sim_score=-9
		}else{
			$final_sim_score=  ($sum*($shorter_array_len+1)+$sum*($longer_array_len+1))/(2*($shorter_array_len+1)*($longer_array_len+1))
		}

printf "SIM SCORE = %.3f\n",$final_sim_score;

printf OUTPUT "%.3f\n",$final_sim_score;



		if ($final_sim_score>=$min_sim_score)
		{
        	#print OUTPUT "1\n";		
			$greater=1;
		}
		else
		{
			#print OUTPUT "0\n";			
			$greater=0;
		}
	
	if ($greater==1) {$tot_count++;}
	
   }
print "\nTOTAL MATCH $tot_count\n";

close(TYPE_FREQ_INPUT);

########## END HERE
#################################################
#####################################################
sub RemoveAnArrayElem
{
	my($arr,$rem)=($_[0],$_[1]);
	my $i=0;
	my @res;

	while ($i<@$arr)
	{
		if ($arr->[$i] eq $rem)
		{
			while ($i<@$arr-1)
			{
				$res[$i]=$arr->[$i+1];
				$i++;
			}			
			
		}
		else {$res[$i]=$arr->[$i];}

		$i++;
	}
	return @res;
}

####################################################
sub FindToken 
{	my($str)=@_;
	my(@token);
	my $i=0;
    	while ($str=~/\S+/g)
		{
			$tok=$&;
			@token[$i]=$tok;
			$i++;
		}	
	
 	return @token;
}

#####################################################

