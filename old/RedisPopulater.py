import regex as re
import redis


class RedisBase(object):

    def __init__(self):
        self._connect_to_redis()

    def _connect_to_redis(self):
        self.r = redis.StrictRedis(host='localhost', port=6379, db=0)
        self.p = self.r.pipeline()


class RedisPopulater(RedisBase):

    def __init__(self):
        self.gram_pattern = re.compile(r'3gm-(\d{4})')
        super(RedisPopulater, self).__init__()
        pass

    def import_vocab(self, file_name):
        with open(file_name) as f:

            # Initialize counters to avoid calls to len
            command_count = 0
            uni_dict = {}
            uni_dict_count = 0

            # For each line in the file
            for line in f:

                # Split the line except the newline character on whitespace and give things names we understand
                data = line[:-1].split()
                unigram = data[0]
                frequency = data[1]

                # Create a key to be used in redis with the unigram name after v. where v. will go before all vocab
                key = "u.{}".format(unigram)

                # Add the key, frequency pair to the dictionary that holds unigrams until they are piped
                uni_dict[key] = frequency
                uni_dict_count += 1

                # If there have been a multiple of 100 unigrams in the dictionary, make an mset and put it in the
                # pipeline then reset the dictionary
                if uni_dict_count % 100 == 0:
                    self.p.mset(uni_dict)
                    uni_dict = {}
                    command_count += 1

                # if there are a multiple of 100 commands in the pipeline, send everything in the pipeline to redis
                if command_count % 100 == 0:
                    self.p.execute()

            # Send the rest of the stuff that is in a dict but wouldn't be pushed because the last data wasn't a
            # multiple of 100
            self.p.mset(uni_dict)
            self.p.execute()

    def import_3gms(self, file_name):
        gram_file = self.gram_pattern.search(file_name).groups()[0]
        with open(file_name) as f:
            i = 0
            for line in f:
                data = line.split()
                word_1 = data[0]
                word_2 = data[1]
                word_3 = data[2]
                frequency = data[3]

                # You need to change this to instead check if the first and third words can appear based on the filter
                # conditions in the convert_to_good.py file.
                # The second word needs to be checked against the pattern they provided (alphanumeric characters only)
                # also, this needs to maintain the bug about filenames
                if True:
                    key = "t.{}_{}".format(gram_file, word_1)
                    self.p.hincrby(key, word_3, frequency)
                    i += 1
                    if i % 10000 == 0:
                        print(key)
                        self.p.execute()
            self.p.execute()

if __name__ == "__main__":
    rp = RedisPopulater()
    rp.import_vocab("D:\\3gms\\vocab")
    print("Imported Vocab")

    for i in range(98):
        file_name = "D:\\3gms\\3gm-{:<04d}".format(i)
        rp.import_3gms(file_name=file_name)
        print("Imported %s" % file_name)