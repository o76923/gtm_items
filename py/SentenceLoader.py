from nltk.corpus import stopwords
import re

BAD_PATTERN = re.compile(r'[^A-Za-z ]+')
STOPWORDS = set(stopwords.words('english')).union({'someone', 'keep', 'along', 'contains', 'something', 'next',
                                                   'together', 'near', 'sometimes', 'example', 'either', 'provides',
                                                   'used', 'could', 'without', 'former', 'particular', 'another',
                                                   'one', 'indicate', 'says', 'often', 'way', 'name', 'use'})


def clean_sentence(sentence_text):
    return [w for w in BAD_PATTERN.sub("", sentence_text.lower().replace("'", " ")).split()
            if w not in STOPWORDS and len(w) > 1]


def load_sentences(sentence_file):
    with open(sentence_file, encoding="utf-8-sig") as f:
        sentences = {int(k): clean_sentence(v) for k, v in
                     [line.split("\t", 2) for line in f.readlines() if "\t" in line]}
    return sentences
