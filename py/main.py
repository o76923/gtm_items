from itertools import combinations
from py.SimScorer import FileScorer
from py.SentenceLoader import load_sentences

raw_sentence_file = "/app/data/sentences.txt"
sim_file = "/app/data/sims.txt"
ngram_dir = "/app/Web_1T/"

if __name__ == "__main__":
    sentences = load_sentences(raw_sentence_file)
    print("Loaded Sentences")

    fs = FileScorer(ngram_dir)
    fs._debug_options = [
        "WordsPresent",
        "WordsRemoved",
        # "MatrixCreated",
        # "MeansCalculated",
        # "WordsAboveThreshold",
        # "AverageAboveThreshold",
        # "SimilarityList",
        "FinalScore",
        "VocabLoaded",
        "3gmLoaded",
    ]
    fs.load_vocab()

    with open(sim_file, "w") as o:
        for left_id, right_id in combinations(sentences, 2):
            try:
                row = "{},{},{}".format(left_id, right_id, fs.sentence_sim(sentences[left_id], sentences[right_id]))
                print(row)
                o.write(row+"\n")
            except Exception as e:
                print(e)

