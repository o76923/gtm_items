# coding=utf-8

from itertools import product
from collections import Counter
from math import log  # log base e, which is correct, by default
import regex as re2  # This is not the standard regex library for python
import pandas as pd


def _find_3gm(word):
    """Find which 3gm file the word comes from.

    Note: the logical error of it not completely loading words that span the boundary between two files is preserved
    from the original perl code. It is possible that the web version has been corrected.

    :param word: the word you want to locate the file of
    :return: the name of the 3gm file or None if the word isn't legal
    """
    if "& UnifyPow \"	" < word < "( Men were	":
        file_name = "3gm-0001"
    elif "( Men who	" < word < "+ * config.sub	":
        file_name = "3gm-0002"
    elif "+ * config.table	" < word < ", Mary Alvarez	":
        file_name = "3gm-0003"
    elif ", Mary Alvina	" < word < ", nude hendrix	":
        file_name = "3gm-0004"
    elif ", nude hentai	" < word < "- Farr 's	":
        file_name = "3gm-0005"
    elif "- Farr (	" < word < "- relational engine	":
        file_name = "3gm-0006"
    elif "- relational environment	" < word < "/ songs )	":
        file_name = "3gm-0007"
    elif "/ songs *	" < word < "10 January --	":
        file_name = "3gm-0008"
    elif "10 January .	" < word < "180 4128 </S>	":
        file_name = "3gm-0009"
    elif "180 4128 |	" < word < "2006 Chocolate -	":
        file_name = "3gm-0010"
    elif "2006 Chocolate /	" < word < "397KB PDF )	":
        file_name = "3gm-0011"
    elif "397KB ] </S>	" < word < "7 Ãº </S>	":
        file_name = "3gm-0012"
    elif "7 Ã» ;	" < word < ": DarkCompass </S>	":
        file_name = "3gm-0013"
    elif ": DarkCounter </S>	" < word < "<S> 1006 Hwy	":
        file_name = "3gm-0014"
    elif "<S> 1006 KB	" < word < "<S> Bell officials	":
        file_name = "3gm-0015"
    elif "<S> Bell often	" < word < "<S> Gloss measurements	":
        file_name = "3gm-0016"
    elif "<S> Gloss medium	" < word < "<S> OUTLET <UNK>	":
        file_name = "3gm-0017"
    elif "<S> OUTLET ADS	" < word < "<S> Thinker <UNK>	":
        file_name = "3gm-0018"
    elif "<S> Thinker >	" < word < "<S> nent military	":
        file_name = "3gm-0019"
    elif "<S> nent model	" < word < "<UNK> Coastpath :	":
        file_name = "3gm-0020"
    elif "<UNK> Coastpath Nearby	" < word < "A MEMBER and	":
        file_name = "3gm-0021"
    elif "A MEMBER box	" < word < "Aitchison ( Oxford	":
        file_name = "3gm-0022"
    elif "Aitchison ( Sat	" < word < "Auckland Festival 05	":
        file_name = "3gm-0023"
    elif "Auckland Festival </S>	" < word < "Bingo - Help	":
        file_name = "3gm-0024"
    elif "Bingo - Here	" < word < "CEOS Events FAQs	":
        file_name = "3gm-0025"
    elif "CEOS FORM COALITION	" < word < "Chicago Membership News	":
        file_name = "3gm-0026"
    elif "Chicago Membership Posted	" < word < "CopÃ¡n Mosaics Project	":
        file_name = "3gm-0027"
    elif "CopÃ¡n Ruinas (	" < word < "Deep & Simple	":
        file_name = "3gm-0028"
    elif "Deep & Skin	" < word < "ESSENTIALS II </S>	":
        file_name = "3gm-0029"
    elif "ESSENTIALS IN ONE	" < word < "FOAM - 888	":
        file_name = "3gm-0030"
    elif "FOAM - <UNK>	" < word < "From southeast corner	":
        file_name = "3gm-0031"
    elif "From southeast star	" < word < "GualeguaychÃº ( Argentina	":
        file_name = "3gm-0032"
    elif "GualeguaychÃº ( ar	" < word < "Hotel In Kurdistan	":
        file_name = "3gm-0033"
    elif "Hotel In Kusadasi	" < word < "Installing xerces -	":
        file_name = "3gm-0034"
    elif "Installing xforms .	" < word < "KIDS DESIGN Current	":
        file_name = "3gm-0035"
    elif "KIDS DESIGNER CLOTHES	" < word < "Lexapro 10/31/05 (	":
        file_name = "3gm-0036"
    elif "Lexapro 10mg (	" < word < "Making Noise '	":
        file_name = "3gm-0037"
    elif "Making Noise (	" < word < "Moment With Gael	":
        file_name = "3gm-0038"
    elif "Moment With God	" < word < "Nice ( about	":
        file_name = "3gm-0039"
    elif "Nice ( adamr.	" < word < "Our Honesty Policy	":
        file_name = "3gm-0040"
    elif "Our Honey &	" < word < "Phaser 350 Phaser	":
        file_name = "3gm-0041"
    elif "Phaser 350 Tektronix	" < word < "Pullets : Number	":
        file_name = "3gm-0042"
    elif "Pullets ; </S>	" < word < "Residency program are	":
        file_name = "3gm-0043"
    elif "Residency program at	" < word < "Sabatini , D.	":
        file_name = "3gm-0044"
    elif "Sabatini , DA	" < word < "Simple Guestbook 2.0	":
        file_name = "3gm-0045"
    elif "Simple Guestbook </S>	" < word < "Students realize they	":
        file_name = "3gm-0046"
    elif "Students realized that	" < word < "Tests mit dem	":
        file_name = "3gm-0047"
    elif "Tests module for	" < word < "Tool Dallas Free	":
        file_name = "3gm-0048"
    elif "Tool Dallas Garden	" < word < "VPD , SQL	":
        file_name = "3gm-0049"
    elif "VPD , and	" < word < "White Leather G	":
        file_name = "3gm-0050"
    elif "White Leather Gift	" < word < "] Logging questions	":
        file_name = "3gm-0051"
    elif "] Logging remote	" < word < "academic and test	":
        file_name = "3gm-0052"
    elif "academic and testing	" < word < "also need compliance	":
        file_name = "3gm-0053"
    elif "also need comprehensive	" < word < "and democratizing ,	":
        file_name = "3gm-0054"
    elif "and democratizing .	" < word < "approached Lena ,	":
        file_name = "3gm-0055"
    elif "approached Lenin after	" < word < "auctions are insured	":
        file_name = "3gm-0056"
    elif "auctions are intended	" < word < "between , public	":
        file_name = "3gm-0057"
    elif "between , pure	" < word < "by : patrikbeno	":
        file_name = "3gm-0058"
    elif "by : patriot	" < word < "ceramics chess sets	":
        file_name = "3gm-0059"
    elif "ceramics chlorine free	" < word < "companion parrots possible	":
        file_name = "3gm-0060"
    elif "companion part ,	" < word < "cracking programs (	":
        file_name = "3gm-0061"
    elif "cracking programs )	" < word < "deserved praise and	":
        file_name = "3gm-0062"
    elif "deserved praise as	" < word < "driveway is permitted	":
        file_name = "3gm-0063"
    elif "driveway is plowed	" < word < "error executing this	":
        file_name = "3gm-0064"
    elif "error executing wwmaster.initiate	" < word < "feet secretary sex	":
        file_name = "3gm-0065"
    elif "feet secretary stockings	" < word < "for nude lesbianas	":
        file_name = "3gm-0066"
    elif "for nude lesbians	" < word < "gas card ...	":
        file_name = "3gm-0067"
    elif "gas card 0	" < word < "hardcore XXX time	":
        file_name = "3gm-0068"
    elif "hardcore XXX video	" < word < "hotels , ideally	":
        file_name = "3gm-0069"
    elif "hotels , ideas	" < word < "in states directly	":
        file_name = "3gm-0070"
    elif "in states do	" < word < "iorque + saudades	":
        file_name = "3gm-0071"
    elif "iorr.org , yesterday	" < word < "la presiÃ³n <UNK>	":
        file_name = "3gm-0072"
    elif "la presiÃ³n a	" < word < "local buildings or	":
        file_name = "3gm-0073"
    elif "local buildings such	" < word < "means officers are	":
        file_name = "3gm-0074"
    elif "means officers can	" < word < "moxie \" .	":
        file_name = "3gm-0075"
    elif "moxie \" and	" < word < "not have NBC	":
        file_name = "3gm-0076"
    elif "not have NC	" < word < "of emotionally laden	":
        file_name = "3gm-0077"
    elif "of emotionally loaded	" < word < "one cause as	":
        file_name = "3gm-0078"
    elif "one cause at	" < word < "output is added	":
        file_name = "3gm-0079"
    elif "output is additionally	" < word < "photos acteress ,	":
        file_name = "3gm-0080"
    elif "photos acteress indian	" < word < "priced proposal </S>	":
        file_name = "3gm-0081"
    elif "priced proposal and	" < word < "raised during 1999	":
        file_name = "3gm-0082"
    elif "raised during 2001	" < word < "research materials )	":
        file_name = "3gm-0083"
    elif "research materials *	" < word < "school right as	":
        file_name = "3gm-0084"
    elif "school right at	" < word < "shows old power	":
        file_name = "3gm-0085"
    elif "shows old school	" < word < "spermine , a	":
        file_name = "3gm-0086"
    elif "spermine , and	" < word < "supplement and advance	":
        file_name = "3gm-0087"
    elif "supplement and after	" < word < "that includes accomplishments	":
        file_name = "3gm-0088"
    elif "that includes account	" < word < "the documents transcribed	":
        file_name = "3gm-0089"
    elif "the documents transferred	" < word < "then will reduce	":
        file_name = "3gm-0090"
    elif "then will refer	" < word < "to access Payroll	":
        file_name = "3gm-0091"
    elif "to access Penn	" < word < "trials will look	":
        file_name = "3gm-0092"
    elif "trials will make	" < word < "valid debts may	":
        file_name = "3gm-0093"
    elif "valid debts of	" < word < "well as brushless	":
        file_name = "3gm-0094"
    elif "well as brutal	" < word < "with two mechanisms	":
        file_name = "3gm-0095"
    elif "with two mechanized	" < word < "| Finite Element	":
        file_name = "3gm-0096"
    elif "| Finite IT	" < word < "É É É	":
        file_name = "3gm-0097"
    elif "! \" ''	" < word < "& Unify Our	":
        file_name = "3gm-0000"
    else:
        raise RuntimeWarning("{} was not found in any 3gm file".format(word))
    return file_name


class SimScorer(object):
    the_frequency = 19401194714.0
    the_squared_frequency = 19401194714.0 ** 2

    def __init__(self):
        self.word_pairs = None
        self.current_pair_no = 0
        self._debug_options = []

    def sentence_sim(self, left_tokens, right_tokens):
        # Swap so that the shorter list is in left_tokens
        if len(left_tokens) > len(right_tokens):
            left_tokens, right_tokens = right_tokens, left_tokens

        # Note the starting length of each string after swapping so left is shorter
        left_length = len(left_tokens)
        right_length = len(right_tokens)

        # DEBUG - check that all words are present
        if "WordsPresent" in self._debug_options:
            print(left_tokens, right_tokens)

        # Remove and count exact word matches
        # This removes duplicates from left_tokens and assigns right_tokens to be all tokens not in right_tokens
        # so you don't need a corresponding change to left_tokens
        right_tokens = [rw for rw in right_tokens if rw not in left_tokens or left_tokens.remove(rw)]
        word_pair_scores = [1.0] * (left_length - len(left_tokens))

        # DEBUG - check that words are removed properly
        if "WordsRemoved" in self._debug_options:
            print(left_tokens, right_tokens)

        # Create a DataFrame for the sentence similarity matrix
        # Word indices are used instead of just the words to account for words repeated in the same sentence
        # <NULL> is in quotes in order to make sure that they are initialized with the correct data type
        all_word_pairs = [p for p in product(range(len(left_tokens)), range(len(right_tokens)))]
        sentence_similarity_matrix = pd.DataFrame(all_word_pairs, columns=["left_word_index", "right_word_index"])
        sentence_similarity_matrix["left_word"] = "<NULL>"
        sentence_similarity_matrix["right_word"] = "<NULL>"
        sentence_similarity_matrix["sim"] = 0.0
        sentence_similarity_matrix["threshold"] = 0.0
        sentence_similarity_matrix = sentence_similarity_matrix.set_index(["left_word_index", "right_word_index"])

        # Populate Sentence Similarity Matrix
        for p in all_word_pairs:
            left_word = left_tokens[p[0]]
            right_word = right_tokens[p[1]]
            sim = self.word_sim(left_word, right_word)
            sentence_similarity_matrix.loc[p, "sim"] = sim
            sentence_similarity_matrix.loc[p, "left_word"] = left_word
            sentence_similarity_matrix.loc[p, "right_word"] = right_word

        # DEBUG - check that sentence similarity matrix is correctly calculated
        if "MatrixCreated" in self._debug_options:
            print(sentence_similarity_matrix)

        # Calculate mean and standard deviation for each word
        left_grouped = sentence_similarity_matrix.groupby(level=["left_word_index", ])
        means = left_grouped.mean()
        deviation = left_grouped.std(ddof=0)

        # DEBUG - check that mean and std are properly calculated
        if "MeansCalculated" in self._debug_options:
            print(left_grouped["sim"])
            print(means, deviation)

        # Create a threshold column
        for g in left_grouped.groups:
            sentence_similarity_matrix.loc[left_grouped.groups[g], "threshold"] = means["sim"][g] + deviation["sim"][g]

        # Create groups for words above the threshold and calculate their averages
        above_threshold = sentence_similarity_matrix.query('sim >= threshold').groupby(level=["left_word_index", ])
        avg_above_threshold = above_threshold.mean()

        # DEBUG - check that only words above threshold are printed
        if "WordsAboveThreshold" in self._debug_options:
            print(above_threshold)

        if "AverageAboveThreshold" in self._debug_options:
            print(avg_above_threshold)

        # Add average above threshold to the list of 1.0s from identical words
        word_pair_scores = word_pair_scores + avg_above_threshold["sim"].tolist()

        # DEBUG - check word similarity list
        if "SimilarityList" in self._debug_options:
            print(word_pair_scores)

        # Normalize their sum by multiplying by the reciprocal of the harmonic mean of the lengths
        final = (sum(word_pair_scores) * (left_length + right_length)) / (2.0 * (left_length * right_length))

        # DEBUG - check final similarity score
        if "FinalScore" in self._debug_options:
            print(final)

        return final

    def word_sim(self, left_word, right_word):
        raise NotImplementedError

    def sim_from_frequencies(self, left_unigram_frequency, right_unigram_frequency, left_right_frequency,
                             right_left_frequency):

        # if one of the words doesn't exist, or neither trigram exists return a similarity of 0.0
        if left_unigram_frequency == 0.0 or right_unigram_frequency == 0.0 or (
                        left_right_frequency == 0.0 and right_left_frequency == 0.0):
            return 0.0

        # find the smaller and larger of the unigram frequencies
        min_unigram = min(left_unigram_frequency, right_unigram_frequency)
        max_unigram = max(left_unigram_frequency, right_unigram_frequency)

        # find the average of the trigram frequencies
        avg_trigram = (left_right_frequency + right_left_frequency) / 2

        # calculate numerator
        numerator = (avg_trigram * self.the_squared_frequency) / (
            left_unigram_frequency * right_unigram_frequency * min_unigram)

        # since you divide by the log of the numerator, values under 1 create division by 0 errors
        if numerator <= 1.0:
            numerator = 1.01

        return log(numerator) / (-2 * log(min_unigram / self.the_frequency))


class FileScorer(SimScorer):
    def __init__(self, ngram_dir):
        self.ngram_dir = ngram_dir
        self.unigram_frequency = Counter()
        self.trigrams = dict()
        self.trigram_load_pattern = re2.compile(r'^([^ ]*) ([^ ]*) ([^\t]*)\t(\d*)')
        self.middle_token_pattern = re2.compile(r'^\p{posix_alnum}*$', re2.UNICODE)
        super(FileScorer, self).__init__()

    def load_vocab(self):
        with open("%s/1gms/vocab" % self.ngram_dir, encoding="utf-8") as f:
            for line in f:
                data = line[:-1].split("\t")
                if len(data) == 2:
                    self.unigram_frequency[data[0]] = int(data[1])
        if "VocabLoaded" in self._debug_options:
            print("Vocab Loaded")

    def word_sim(self, left_word, right_word):
        # Get the unigram count of the words
        left_unigram_frequency = self.unigram_frequency[left_word]
        right_unigram_frequency = self.unigram_frequency[right_word]

        # Find which files contain the relevant trigrams
        left_3gm_file = _find_3gm(left_word)
        right_3gm_file = _find_3gm(right_word)

        # Load the relevant trigrams if they haven't been loaded yet
        if left_3gm_file not in self.trigrams:
            self._load_3gm(left_3gm_file)
        if right_3gm_file not in self.trigrams:
            self._load_3gm(right_3gm_file)

        # Return the frequencies for the trigram in either direction
        try:
            left_right_frequency = self.trigrams[left_3gm_file][left_word][right_word]
        except KeyError:
            left_right_frequency = 0.0

        try:
            right_left_frequency = self.trigrams[right_3gm_file][right_word][left_word]
        except KeyError:
            right_left_frequency = 0.0

        return self.sim_from_frequencies(left_unigram_frequency, right_unigram_frequency, left_right_frequency,
                                         right_left_frequency)

    def word_sim_parts(self, left_word, right_word):
        # Get the unigram count of the words
        left_unigram_frequency = self.unigram_frequency[left_word]
        right_unigram_frequency = self.unigram_frequency[right_word]

        # Find which files contain the relevant trigrams
        left_3gm_file = _find_3gm(left_word)
        right_3gm_file = _find_3gm(right_word)

        # Load the relevant trigrams if they haven't been loaded yet
        if left_3gm_file not in self.trigrams:
            self._load_3gm(left_3gm_file)
        if right_3gm_file not in self.trigrams:
            self._load_3gm(right_3gm_file)

        # Return the frequencies for the trigram in either direction
        left_right_frequency = self.trigrams[left_3gm_file][left_word][right_word]
        right_left_frequency = self.trigrams[right_3gm_file][right_word][left_word]

        return left_unigram_frequency, right_unigram_frequency, left_right_frequency, right_left_frequency

    def _load_3gm(self, file_name):
        # create the dict of left words per file
        self.trigrams[file_name] = dict()

        # load the 3gm file in unicode
        with open("%s/3gms/%s" % (self.ngram_dir, file_name), encoding='utf-8') as f:

            # iterate through each line
            for line in f:

                # break the line into groups based on their pattern for lines to use
                matches = self.trigram_load_pattern.match(line)

                # only continue if the line meets the pattern provided
                if matches:

                    # turn the matching groups into a tuple
                    data = matches.groups()

                    # if the middle word is only letters and numbers, increment the counter
                    if self.middle_token_pattern.match(data[1]):

                        # if there isn't a counter, make one for the left word
                        if data[0] not in self.trigrams[file_name]:
                            self.trigrams[file_name][data[0]] = Counter()

                        # increment the counter. Note that counters return 0 for keys not in them.
                        self.trigrams[file_name][data[0]][data[2]] += float(data[3])

        if "3gmLoaded" in self._debug_options:
            print("{} loaded".format(file_name))
